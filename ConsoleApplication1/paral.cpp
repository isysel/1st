#include "stdafx.h"
#include "paral.h"
#include <fstream>
using namespace std;

paral *InParal(ifstream &ifst)
{
	paral *add = new paral();
	ifst >> add->a >> add->b >> add->c;
	return add;
}

void OutParal(paral *a, ofstream &ofst)
{
	ofst << "2\n" << a->a << " " << a->b << " " << a->c << "\n\n";
}